﻿using Unity.Mathematics;

public static class Extensions
{
    public static float3 RotateByAngleRad (this float3 v, float alpha)
    {
        v = new float3(
            v.x * math.cos(alpha) - v.y * math.sin(alpha),
            v.x * math.sin(alpha) + v.y * math.cos(alpha),
            v.z);
        return v;
    }
    public static float3 RotateByAngle (this float3 vector, float alpha)
    {
        return vector.RotateByAngleRad(math.radians(alpha));
    }
}
