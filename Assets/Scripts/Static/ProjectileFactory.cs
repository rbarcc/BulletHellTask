﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public static class ProjectileFactory
{
    static EntityManager _em;
    static ProjectileFactory()
    {
        _em = World.DefaultGameObjectInjectionWorld.EntityManager;
    }
    public static void SpawnProjectiles(Entity template, float3 spawnPosition,
        float3 direction, ProjectileParamsData projParams, bool isForked = false)
    {
        int totalProjectiles = 1 + projParams.ExtraProjectiles;

        if (totalProjectiles <= 0) return;
        if (totalProjectiles == 1)
        {
            SpawnProjectile(template, spawnPosition, direction, projParams);
            return;
        }

        float angle = projParams.Angle;
        float3 leftMostDirection = direction.RotateByAngle(angle / 2f);

        float increment = angle / (totalProjectiles-1);

        // We'll start at direction rotated by angle / 2 to the left
        // and then we'll rotate each projectile by increment to the right
        for (int i = 0; i < totalProjectiles; i++)
        {
            float rotation = -i * increment;
            // then rotate it by rotation (0 for 1st increment)
            float3 launchDirection = leftMostDirection.RotateByAngle(rotation);
            SpawnProjectile(template, spawnPosition, launchDirection, projParams,
                isForked);
        }
    }

    public static void SpawnProjectile(Entity template, float3 spawnPosition,
        float3 direction, ProjectileParamsData projParams, bool isForked = false)
    {
        Entity copy = _em.Instantiate(template);
        if (_em.HasComponent<InactiveTag>(copy))
            _em.RemoveComponent<InactiveTag>(copy);
        if (isForked)
        {
            if (_em.HasComponent<ForkingEffectData>(copy))
                _em.RemoveComponent<ForkingEffectData>(copy);
            if (_em.HasComponent<FollowData>(copy))
                _em.RemoveComponent<FollowData>(copy);
        }
        _em.SetComponentData(copy, new DirectionData()
        {
            Value = direction
        });
        _em.SetName(copy, "Player Projectile (copy)");
    }
}
