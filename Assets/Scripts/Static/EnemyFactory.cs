﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

namespace Game
{
    public static class EnemyFactory
    {
        static EntityManager _em;
        static EnemyFactory()
        {
            _em = World.DefaultGameObjectInjectionWorld.EntityManager;
        }
        public static void SpawnEnemyAroundCircle(float3 spawnerPosition, 
            EnemySpawnerData spawnerData)
        {
            float3 randomDirection =
                new float3(UnityEngine.Random.Range(-1f, 1f),
                           UnityEngine.Random.Range(-1f, 1f),
                           0f);
            randomDirection = math.normalize(randomDirection);
            float randomRadius = UnityEngine.Random.Range(spawnerData.InnerRadius,
                spawnerData.OuterRadius);
            float3 spawnSpot = new float3(
                spawnerPosition.x + randomDirection.x * randomRadius,
                spawnerPosition.y + randomDirection.y * randomRadius,
                0);
            SpawnEnemy(spawnSpot, spawnerData);
        }
        public static void SpawnEnemy(float3 spawnSpot, EnemySpawnerData spawnerData)
        {
            var enemy = _em.CreateEntity(
                typeof(EnemyTag),
                typeof(Translation),
                typeof(HealthData)
            );
            _em.SetComponentData(enemy, new Translation()
            {
                Value = spawnSpot
            });
            _em.SetComponentData(enemy, new HealthData()
            {
                Value = spawnerData.EnemyHealth
            });
            _em.SetName(enemy, "Enemy");
        }
    }
}
