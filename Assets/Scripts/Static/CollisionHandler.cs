﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using System.Linq;

public static class CollisionHandler
{
    static EntityManager _em;
    static EndSimulationEntityCommandBufferSystem _endSimulationBufferSystem;

    static CollisionHandler()
    {
        _em = World.DefaultGameObjectInjectionWorld.EntityManager;
        _endSimulationBufferSystem = World.DefaultGameObjectInjectionWorld
            .GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
    public static void CollideProjectileWithAnEnemy (Entity projectile, Entity enemy)
    {
        HealthData enemyHealth = _em.GetComponentData<HealthData>(enemy);
        DamageData projectileDamage = _em.GetComponentData<DamageData>(projectile);

        DynamicBuffer<BlacklistBuffer> updatedBlacklist;
        UpdateBlacklist(projectile, enemy, out updatedBlacklist);
        
        bool isChaining = _em.HasComponent<ChainingEffectData>(projectile);
        bool isPiercing = _em.HasComponent<PiercingEffectData>(projectile);
        bool isForking = _em.HasComponent<ForkingEffectData>(projectile);

        int finalEnemyHealth = enemyHealth.Value - (int)math.round(projectileDamage.Value);
        _em.SetComponentData(enemy, new HealthData()
        {
            Value = math.max(0, finalEnemyHealth)
        });

        // You can change the priority of forking/piercing/chaining here
        if (isForking)
        {
            float3 direction = _em.GetComponentData<DirectionData>(projectile).Value;
            ForkingEffectData forkEffect = _em.GetComponentData<ForkingEffectData>(projectile);
            float3 projPosition = _em.GetComponentData<Translation>(projectile).Value;
            ProjectileParamsData projParams = new ProjectileParamsData()
            {
                Angle = 90,
                ExtraProjectiles = forkEffect.Projectiles
            };
            ProjectileFactory.SpawnProjectiles(projectile, projPosition,
                direction, projParams, true);
            EntityCommandBuffer ecb = _endSimulationBufferSystem.CreateCommandBuffer();
            ecb.DestroyEntity(projectile);
        }
        else if (isPiercing) HandlePiercing(projectile);
        else if (isChaining) HandleChaining(projectile, updatedBlacklist);
        else
        {
            EntityCommandBuffer ecb = _endSimulationBufferSystem.CreateCommandBuffer();
            ecb.DestroyEntity(projectile);
        }
    }

    private static void HandleChaining(Entity projectile, 
        DynamicBuffer<BlacklistBuffer> blacklist)
    {
        ChainingEffectData chaining = _em.GetComponentData<ChainingEffectData>(projectile);

        if (chaining.RemainingTargets == 0)
        {
            EntityCommandBuffer ecb = _endSimulationBufferSystem.CreateCommandBuffer();
            ecb.DestroyEntity(projectile);
        }
        else
        {
            _em.SetComponentData(projectile, new ChainingEffectData()
            {
                RemainingTargets = chaining.RemainingTargets - 1
            });
            EntityQuery eQ = _em.CreateEntityQuery(typeof(EnemyTag), typeof(Translation));
            var allEnemies = eQ.ToEntityArray(Allocator.Temp);
            float minDistance = 999999;
            Entity minDistanceEnt = Entity.Null;
            float3 projectilePos = _em.GetComponentData<Translation>(projectile).Value;

            // Find closest enemy that isn't blacklisted
            for (int j = 0; j < allEnemies.Length; j++)
            {
                bool isOnTheBlacklist = false;
                for (int i = 0; i < blacklist.Length; i++)
                {
                    if (allEnemies[j] == blacklist[i].Value)
                        isOnTheBlacklist = true;
                }
                if (isOnTheBlacklist) continue;

                float3 enemyPos = _em.GetComponentData<Translation>(allEnemies[j]).Value;
                float distance = math.distance(enemyPos, projectilePos);
                if (distance < minDistance)
                {
                    minDistanceEnt = allEnemies[j];
                    minDistance = distance;
                }
            }
            if (minDistanceEnt != Entity.Null)
            {
                _em.AddComponent(projectile, typeof(FollowData));
                _em.SetComponentData(projectile, new FollowData()
                {
                    Target = minDistanceEnt
                });
            }
        }
    }

    private static void HandlePiercing(Entity projectile)
    {
        PiercingEffectData piercing = _em.GetComponentData<PiercingEffectData>(projectile);
        if (piercing.RemainingTargets == 0)
            _em.RemoveComponent(projectile, typeof(PiercingEffectData));
        else
            _em.SetComponentData(projectile, new PiercingEffectData()
            {
                RemainingTargets = piercing.RemainingTargets - 1
            });
    }

    private static void UpdateBlacklist(Entity projectile, Entity enemy, 
        out DynamicBuffer<BlacklistBuffer> bList)
    {
        DynamicBuffer<BlacklistBuffer> blacklist;
        if (!_em.HasComponent<BlacklistBuffer>(projectile))
            blacklist = _em.AddBuffer<BlacklistBuffer>(projectile);
        else
            blacklist = _em.GetBuffer<BlacklistBuffer>(projectile);
        blacklist.Add(new BlacklistBuffer { Value = enemy });
        bList = blacklist;
    }
}
