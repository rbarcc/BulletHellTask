﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct RadiusData : IComponentData
{
    public float Value;
}

