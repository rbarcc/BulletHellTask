﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct PiercingEffectData : IComponentData
{
    public int RemainingTargets;
}
