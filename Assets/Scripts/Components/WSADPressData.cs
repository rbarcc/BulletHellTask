﻿using Unity.Entities;

public struct WSADPressData : IComponentData
{
    public bool W, S, A, D;
}
