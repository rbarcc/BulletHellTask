﻿using Unity.Entities;

public struct MousePressData : IComponentData
{
    public bool LMB;
    public bool RMB;
}
