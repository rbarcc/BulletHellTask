﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct CooldownData : IComponentData
{
    public float MaxCooldown;
    public float CurrentCooldown;
}
