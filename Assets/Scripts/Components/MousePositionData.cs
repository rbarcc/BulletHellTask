﻿using Unity.Entities;
using Unity.Mathematics;

public struct MousePositionData : IComponentData
{
    public float2 Value; 
}
