﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct ProjectileTemplateData : IComponentData
{
    public Entity Value;
}
