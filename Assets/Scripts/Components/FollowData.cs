﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct FollowData : IComponentData
{
    public Entity Target;
}
