﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct ProjectileParamsData : IComponentData
{
    [Header("ExtraProjectiles")]
    public int ExtraProjectiles;
    public float Angle;
}
