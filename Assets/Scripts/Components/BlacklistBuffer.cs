﻿using Unity.Entities;

[InternalBufferCapacity(16)]
public struct BlacklistBuffer : IBufferElementData
{
    public Entity Value;
}