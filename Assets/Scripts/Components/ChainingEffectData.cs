﻿using Unity.Collections;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct ChainingEffectData : IComponentData
{
    public int RemainingTargets;
}
