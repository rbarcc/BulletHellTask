﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct ForkingEffectData : IComponentData
{
    public int Projectiles;
}
