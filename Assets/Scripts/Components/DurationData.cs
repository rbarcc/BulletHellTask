﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct DurationData : IComponentData
{
    public float Value;
}
