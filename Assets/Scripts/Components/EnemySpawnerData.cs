﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct EnemySpawnerData : IComponentData
{
    public float InnerRadius;
    public float OuterRadius;
    public int EnemyHealth;
}
