using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace Game
{
    public class GameHandler : MonoBehaviour
    {
        World _world;
        public Mesh quadMesh;
        public Material projectileSprite;
        public Material playerSprite;
        public Material enemySprite;

        private void Awake()
        {
            _instance = this;
            _world = World.DefaultGameObjectInjectionWorld;
            _world.GetExistingSystem<SpawnProjectilesOnPlayerCooldownSystem>().Enabled = false;
            _world.GetExistingSystem<AutomaticEnemySpawnerSystem>().Enabled = false;
        }

        private static GameHandler _instance;
        public static GameHandler GetInstance()
        {
            return _instance;
        }
    }
}
