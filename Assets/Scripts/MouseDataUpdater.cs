﻿using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public class MouseDataUpdater : MonoBehaviour
{
    Camera _cam;
    Entity _mouseEntity;
    EntityManager _em;
    private void Start()
    {
        _cam = Camera.main;
        _em = World.DefaultGameObjectInjectionWorld.EntityManager;
        _mouseEntity = _em.CreateEntity(typeof(MousePositionData),
            typeof(MousePressData),
            typeof(WSADPressData));
        _em.SetName(_mouseEntity, "Mouse");
    }
    private void Update()
    {
        Vector3 screenCoordTemp = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            _cam.nearClipPlane);
        Vector3 point = _cam.ScreenToWorldPoint(screenCoordTemp);
        _em.SetComponentData(_mouseEntity, new MousePositionData
        {
            Value = new float2(point.x, point.y)
        });
        _em.SetComponentData(_mouseEntity, new MousePressData
        {
            RMB = Input.GetMouseButtonDown(1),
            LMB = Input.GetMouseButtonDown(0)
        });
        _em.SetComponentData(_mouseEntity, new WSADPressData
        {
            W = Input.GetKeyDown(KeyCode.W),
            S = Input.GetKeyDown(KeyCode.S),
            A = Input.GetKeyDown(KeyCode.A),
            D = Input.GetKeyDown(KeyCode.D)
        });
    }
    private void LateUpdate()
    {
        _em.SetComponentData(_mouseEntity, new MousePressData
        {
            RMB = false,
            LMB = false
        });
    }
}
