﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct InactiveTag : IComponentData { }
