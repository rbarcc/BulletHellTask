using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class CameraDataUpdater : MonoBehaviour
{
    Camera _cam;
    EntityManager _em;
    Entity _cameraEntity;
    void Start()
    {
        _cam = GetComponent<Camera>();

        var oSize = _cam.orthographicSize;

        //Bounds b = new Bounds(_cam.transform.position,
        //    new Vector2(oSize*2*16/9, oSize*2));

        //Debug.Log(b.center.x + b.size.x);
        //Debug.Log(b.center.y + b.size.y);
        
        _em = World.DefaultGameObjectInjectionWorld.EntityManager;

        _cameraEntity = _em.CreateEntity(typeof(CameraData));
        _em.SetComponentData(_cameraEntity, new CameraData
        {
            OrthographicScale = _cam.orthographicSize,
            Position = _cam.transform.position
        });
        _em.SetName(_cameraEntity, "CameraData");
    }

    private void Update()
    {
        _em.SetComponentData(_cameraEntity, new CameraData
        {
            OrthographicScale = _cam.orthographicSize,
            Position = _cam.transform.position
        }); 
    }
}
