﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Game
{
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class StaticSpriteRenderer : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<InactiveTag>()
                .WithAll<ProjectileTag>().ForEach((ref Translation translation) =>
            {
                Graphics.DrawMesh(
                    GameHandler.GetInstance().quadMesh,
                    translation.Value,
                    Quaternion.identity,
                    GameHandler.GetInstance().projectileSprite,
                    0);
            });
            Entities.WithNone<InactiveTag>()
                .WithAll<PlayerTag>().ForEach((ref Translation translation) =>
            {
                Graphics.DrawMesh(
                    GameHandler.GetInstance().quadMesh,
                    translation.Value,
                    Quaternion.identity,
                    GameHandler.GetInstance().playerSprite,
                    0);
            });
            Entities.WithNone<InactiveTag>()
                .WithAll<EnemyTag>().ForEach((ref Translation translation) =>
            {
                Graphics.DrawMesh(
                    GameHandler.GetInstance().quadMesh,
                    translation.Value,
                    Quaternion.identity,
                    GameHandler.GetInstance().enemySprite,
                    0);
            });
        }
    }
}
