﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Game
{
    public class BounceFromEdgeOfTheScreenSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            float ortoSize = 0;
            float3 camPos = new float3(0, 0, 0);
            Entities.ForEach((ref CameraData cameraData) =>
            {
                ortoSize = cameraData.OrthographicScale;
                camPos = cameraData.Position;
            });

            Entities.WithNone<InactiveTag, FollowData>()
                .WithAll<ProjectileTag>().ForEach((ref Translation translation,
                ref DirectionData direction) =>
            {
                Bounds b = new Bounds(new Vector2(camPos.x, camPos.y), new Vector3(
                        ortoSize * 2 * 16 / 9,
                        ortoSize * 2,
                        0));

                if (!b.Contains(translation.Value))
                {
                    if (translation.Value.x > b.center.x + b.size.x / 2)
                    {
                        // Bounced from the right edge
                        direction.Value.x = -direction.Value.x;
                    }
                    if (translation.Value.x < b.center.x - b.size.x / 2)
                    {
                        // Bounced from the left edge
                        direction.Value.x = -direction.Value.x;
                    }
                    if (translation.Value.y > b.center.y + b.size.y / 2)
                    {
                        // Bounced from the top edge
                        direction.Value.y = -direction.Value.y;
                    }
                    if (translation.Value.y < b.center.y - b.size.y / 2)
                    {
                        // Bounced from the bottom edge
                        direction.Value.y = -direction.Value.y;
                    }
                }
            });
        }
    }
}
