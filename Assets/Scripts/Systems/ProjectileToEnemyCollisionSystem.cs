﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

namespace Game
{
    public class ProjectileToEnemyCollisionSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            EntityManager em = World.DefaultGameObjectInjectionWorld.EntityManager;
            Entities.WithNone<InactiveTag>()
                .WithAll<ProjectileTag>().ForEach((Entity projEnt,
                ref Translation projPos,
                ref RadiusData area) =>
            {
                float circleColliderRadius = area.Value;
                float3 projectilePos = projPos.Value;
                Entities.WithAll<EnemyTag>().ForEach((Entity enemyEnt, 
                    ref Translation enemyPos) =>
                {
                    bool isBlacklisted = false;
                    if (em.HasComponent<BlacklistBuffer>(projEnt))
                    {
                        var blacklist = em.GetBuffer<BlacklistBuffer>(projEnt);
                        for (int i = 0; i < blacklist.Length; i++)
                        {
                            if (blacklist[i].Value == enemyEnt)
                                isBlacklisted = true;
                        }
                    }
                    if (!isBlacklisted)
                        if (math.distance(projectilePos, enemyPos.Value) <= circleColliderRadius)
                            CollisionHandler.CollideProjectileWithAnEnemy(projEnt, enemyEnt);
                });
            });
        }
    }
}
