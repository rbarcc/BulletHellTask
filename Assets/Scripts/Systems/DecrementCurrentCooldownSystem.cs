﻿using Unity.Entities;
using UnityEngine;

namespace Game
{
    public class DecrementCurrentCooldownSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<InactiveTag>()
                .ForEach((ref CooldownData cooldown) =>
            {
                if (cooldown.MaxCooldown <= 0)
                    Debug.LogError("Max cooldown is 0");
                if (cooldown.CurrentCooldown > 0)
                    cooldown.CurrentCooldown -= Time.DeltaTime;
            });
        }
    }
}
