﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using System.Linq;
using UnityEngine;

namespace Game
{
    public class SpawnProjectilesOnPlayerMouseClickSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            float2 mousePosition = new float2(0, 0);
            bool lmbPress = false;

            Entities.ForEach((ref MousePositionData mousePosComponent,
                ref MousePressData mousePress) =>
            {
                mousePosition = mousePosComponent.Value;
                lmbPress = mousePress.LMB;
            });

            Entities.WithNone<InactiveTag>()
                .WithAll<PlayerTag>().ForEach((Entity entity, ref Translation translation,
                ref ProjectileParamsData projParams, ref ProjectileTemplateData template) =>
            {
                if (lmbPress)
                {
                    float2 direction = mousePosition - new float2(translation.Value.x,
                        translation.Value.y);
                    ProjectileFactory.SpawnProjectiles(template.Value, translation.Value,
                        new float3(direction.x, direction.y, 0), projParams);
                }
            });
        }
    }
}
