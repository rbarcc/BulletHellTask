﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game
{
    public class EnemySpawnOnMousePositionSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            bool spawnButtonPressed = false;
            float2 mousePosition = new float2(0, 0);
            Entities.ForEach((ref MousePressData mousePress, ref MousePositionData mousePos) =>
            {
                spawnButtonPressed = mousePress.RMB;
                mousePosition = mousePos.Value;
            });

            Entities.WithNone<InactiveTag>()
                .WithAll<EnemySpawnerTag>().ForEach((ref Translation position,
                ref EnemySpawnerData spawnerData) =>
            {
                if (spawnButtonPressed)
                    EnemyFactory.SpawnEnemy(new float3(
                        mousePosition.x,
                        mousePosition.y, 
                        0), spawnerData);
            });
        }
    }
}
