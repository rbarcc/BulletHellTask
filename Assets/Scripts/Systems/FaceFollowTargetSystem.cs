﻿using Unity.Entities;
using Unity.Transforms;

namespace Game
{
    public class FaceFollowTargetSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            EntityManager em = World.DefaultGameObjectInjectionWorld.EntityManager;
            Entities.WithNone<InactiveTag>()
                .ForEach((ref FollowData follow, ref DirectionData direction,
                ref Translation position) =>
            {
                if (em.Exists(follow.Target))
                {
                    var targetPos = em.GetComponentData<Translation>(follow.Target).Value;
                    direction.Value = targetPos - position.Value;
                }
            });
        }
    }
}
