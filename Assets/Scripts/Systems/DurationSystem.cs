﻿using Unity.Entities;

namespace Game
{
    public class DurationSystem : ComponentSystem
    {
        EndSimulationEntityCommandBufferSystem endSimulationBufferSystem;
        protected override void OnCreate()
        {
            base.OnCreate();
            endSimulationBufferSystem =
                World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }
        protected override void OnUpdate()
        {
            EntityCommandBuffer ecb = endSimulationBufferSystem.CreateCommandBuffer();
            Entities.WithNone<InactiveTag>()
                .ForEach((Entity entity, ref DurationData duration) =>
            {
                duration.Value -= Time.DeltaTime;
                if (duration.Value <= 0)
                    ecb.DestroyEntity(entity);
            });
        }
    }
}
