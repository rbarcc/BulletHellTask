﻿using Unity.Entities;
using Unity.Transforms;

namespace Game
{
    public class AutomaticEnemySpawnerSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<InactiveTag>()
                .WithAll<EnemySpawnerTag>().ForEach((ref CooldownData cooldown,
                ref Translation position, ref EnemySpawnerData spawnerData) =>
            {
                if (cooldown.CurrentCooldown <= 0)
                {
                    EnemyFactory.SpawnEnemyAroundCircle(position.Value, spawnerData);
                    cooldown.CurrentCooldown = cooldown.MaxCooldown;
                }
            });
        }
    }
}
