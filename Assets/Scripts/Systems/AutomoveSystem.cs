﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game
{
    public class AutomoveSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.WithNone<InactiveTag>()
                .WithAll<AutomoveTag>().ForEach((ref Translation translation,
                ref SpeedData speed, ref DirectionData direction) =>
            {
                var dirNormalized = math.normalize(direction.Value);
                translation.Value += dirNormalized * speed.Value * Time.DeltaTime;
            });
        }
    }
}
