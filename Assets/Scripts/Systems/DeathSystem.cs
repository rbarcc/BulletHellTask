﻿using Unity.Entities;

namespace Game
{
    public class DeathSystem : ComponentSystem
    {
        EndSimulationEntityCommandBufferSystem endSimulationBufferSystem;
        protected override void OnCreate()
        {
            base.OnCreate();
            endSimulationBufferSystem =
                World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
        }
        protected override void OnUpdate()
        {
            EntityCommandBuffer ecb = endSimulationBufferSystem.CreateCommandBuffer();
            Entities.WithNone<InactiveTag>().ForEach((Entity entity, ref HealthData health) =>
            {
                if (health.Value <= 0)
                    ecb.DestroyEntity(entity);
            });
        }
    }
}
